package function;

import entity.Stone;
import entity.StoneTypes;
import entity.StoneValueComparator;

import java.util.*;

public class StoneAction extends StoneTypes {
    private List<Stone> listOfStones = new ArrayList<>();

    public void start() {
        Scanner input = new Scanner(System.in);
        String inputNumber = "";
        while (!inputNumber.equals("3")) {
            System.out.println("Please choose option:\n1 - Add gem stone\n2 - Add semi precious stone\n3 - Finish your decoration");
            inputNumber = input.nextLine();
            switch (inputNumber) {
                case "1":
                    addGemStone();
                    break;
                case "2":
                    addSemiStone();
                    break;
                case "3":
                    showList();
                    break;
                default:
                    System.out.println("Invalid command, please repeat");
            }
        }
        if (listOfStones.size() != 0) {
            double lowerbound = 0, upperbound = 0;
            while (true) {
                System.out.print("Please enter range of the transparency :\nLowerbound - ");
                try {
                    lowerbound = Double.parseDouble(input.next());
                    if (!isValidBound(lowerbound)) {
                        throw new IllegalArgumentException();
                    }
                    break;
                } catch (IllegalArgumentException ignore) {
                    System.out.println("Invalid input");
                }
            }
            while (true) {
                System.out.print("Upperbound - ");
                try {
                    upperbound = Double.parseDouble(input.next());
                    if (!isValidBound(upperbound) || upperbound < lowerbound) {
                        throw new IllegalArgumentException();
                    }
                    break;
                } catch (IllegalArgumentException ignore) {
                    System.out.println("Invalid input");
                }
            }
            findStones(lowerbound, upperbound);
        }
    }

    private void addGemStone() {
        EnumSet<GemStones> enumSet = EnumSet.allOf(GemStones.class);
        System.out.println("Please choose one of the stone:");
        for (GemStones gemStone : enumSet) {
            System.out.println(gemStone);
        }
        Scanner input = new Scanner(System.in);
        String inputStone = "";
        while (true) {
            try {
                inputStone = input.nextLine();
                inputStone = inputStone.toUpperCase().trim();
                if (enumSet.contains(GemStones.valueOf(inputStone))) {
                    double weight;
                    while (true) {
                        System.out.print("Please enter weight: ");
                        try {
                            weight = Double.parseDouble(input.next());
                            if (!isValidNumber(weight)) {
                                throw new IllegalArgumentException();
                            }
                            break;
                        } catch (IllegalArgumentException ignore) {
                            System.out.println("Invalid weight, please repeat!");
                        }
                    }
                    listOfStones.add(new Stone(weight, GemStones.valueOf(inputStone)));
                    System.out.println("Stone was added");
                }
                break;
            } catch (IllegalArgumentException ignore) {
                System.out.println("Sorry, but there is no such " + inputStone + " in the list." + "Please write stone name correctly!");
            }
        }
    }

    private void addSemiStone() {
        EnumSet<SemiPreciousStones> enumSet = EnumSet.allOf(SemiPreciousStones.class);
        System.out.println("Please choose one of the stone:");
        for (SemiPreciousStones semiPreciousStone : enumSet) {
            System.out.println(semiPreciousStone);
        }
        Scanner input = new Scanner(System.in);
        String inputStone = "";
        while (true) {
            try {
                inputStone = input.nextLine();
                inputStone = inputStone.toUpperCase().trim();
                if (enumSet.contains(SemiPreciousStones.valueOf(inputStone))) {
                    double weight;
                    while (true) {
                        System.out.print("Please enter weight: ");
                        try {
                            weight = Double.parseDouble(input.next());
                            if (!isValidNumber(weight)) {
                                throw new IllegalArgumentException();
                            }
                            break;
                        } catch (IllegalArgumentException ignore) {
                            System.out.println("Invalid weight, please repeat!");
                        }
                    }
                    listOfStones.add(new Stone(weight, SemiPreciousStones.valueOf(inputStone)));
                    System.out.println("Stone was added");
                }
                break;
            } catch (IllegalArgumentException ignore) {
                System.out.println("Sorry, but there is no such " + inputStone + " in the list." + "Please write stone name correctly!");
            }
        }
    }

    private boolean isValidNumber(double weight) {
        if (weight > 0) {
            return true;
        }
        return false;
    }

    private boolean isValidBound(double bound) {
        if (bound > 0 && bound <= 5) {
            return true;
        }
        return false;
    }

    private void showList() {
        if (listOfStones.size() != 0) {
            listOfStones.sort(new StoneValueComparator());
            double priceOfStone = 0;
            double totalPrice = 0;
            for (Stone stone : listOfStones) {
                if (stone.getGemStone() == null) {
                    priceOfStone = stone.getWeight() * stone.getSemiPreciousStone().getPricePerCarat();
                    System.out.println(stone.getSemiPreciousStone() + " weight = " + stone.getWeight() + " price = " + priceOfStone + " $");
                } else {
                    priceOfStone = stone.getWeight() * stone.getGemStone().getPricePerCarat();
                    System.out.println(stone.getGemStone() + " weight = " + stone.getWeight() + " price = " + priceOfStone + " $");
                }
                totalPrice += priceOfStone;
            }
            System.out.println("Jewelry price = " + totalPrice + " $");
        } else {
            System.out.println("Your list of stones is empty.");
        }
    }

    private void findStones(double lowerbound, double upperbound) {
        int count = 0;
        for (Stone stone : listOfStones) {
            if (stone.getSemiPreciousStone() == null) {
                if (stone.getGemStone().getTransparency() >= lowerbound && stone.getGemStone().getTransparency() <= upperbound) {
                    count++;
                    System.out.println(stone.getGemStone());
                }
            } else {
                if (stone.getSemiPreciousStone().getTransparency() >= lowerbound && stone.getSemiPreciousStone().getTransparency() <= upperbound) {
                    count++;
                    System.out.println(stone.getSemiPreciousStone());
                }
            }
        }
        if (count == 0) {
            System.out.println("There is no stones which pleasures the range.");
        }
    }
}
