package entity;

import java.util.Comparator;

public class StoneValueComparator implements Comparator<Stone> {

    @Override
    public int compare(Stone o1, Stone o2) {
        if (o1.getSemiPreciousStone() == null) {
            if (o2.getGemStone() == null) {
                if (o1.getGemStone().getPricePerCarat() * o1.getWeight() > o2.getSemiPreciousStone().getPricePerCarat() * o2.getWeight()) {
                    return 1;
                } else if (o1.getGemStone().getPricePerCarat() * o1.getWeight() < o2.getSemiPreciousStone().getPricePerCarat() * o2.getWeight()) {
                    return -1;
                }
            }
            if (o1.getGemStone().getPricePerCarat() * o1.getWeight() > o2.getGemStone().getPricePerCarat() * o2.getWeight()) {
                return 1;
            } else if (o1.getGemStone().getPricePerCarat() * o1.getWeight() < o2.getGemStone().getPricePerCarat() * o2.getWeight()) {
                return -1;
            }
        }
        if (o2.getGemStone() == null) {
            if (o1.getSemiPreciousStone().getPricePerCarat() * o1.getWeight() > o2.getSemiPreciousStone().getPricePerCarat() * o2.getWeight()) {
                return 1;
            } else if (o1.getSemiPreciousStone().getPricePerCarat() * o1.getWeight() < o2.getSemiPreciousStone().getPricePerCarat() * o2.getWeight()) {
                return -1;
            }
        }
        if (o1.getSemiPreciousStone().getPricePerCarat() * o1.getWeight() > o2.getGemStone().getPricePerCarat() * o2.getWeight()) {
            return 1;
        } else if (o1.getSemiPreciousStone().getPricePerCarat() * o1.getWeight() < o2.getGemStone().getPricePerCarat() * o2.getWeight()) {
            return -1;
        }
        return 0;
    }
}
