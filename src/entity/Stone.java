package entity;

public class Stone extends StoneTypes {
    private double weight;
    private GemStones gemStone;
    private SemiPreciousStones semiPreciousStone;

    public Stone(double weight, GemStones gemStone) {
        this.weight = weight;
        this.gemStone = gemStone;
    }

    public Stone(double weight, SemiPreciousStones semiPreciousStone) {
        this.weight = weight;
        this.semiPreciousStone = semiPreciousStone;
    }

    public double getWeight() {
        return weight;
    }

    public GemStones getGemStone() {
        return gemStone;
    }

    public SemiPreciousStones getSemiPreciousStone() {
        return semiPreciousStone;
    }
}
