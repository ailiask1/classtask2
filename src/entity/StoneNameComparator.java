package entity;

import java.util.Comparator;

public class StoneNameComparator implements Comparator<Stone> {

    @Override
    public int compare(Stone o1, Stone o2) {
        if (o1.getSemiPreciousStone() == null) {
            if (o2.getGemStone() == null) {
                return o1.getGemStone().name().compareTo(o2.getSemiPreciousStone().name());
            }
            return o1.getGemStone().name().compareTo(o2.getGemStone().name());
        }
        if (o2.getGemStone() == null) {
            return o1.getSemiPreciousStone().name().compareTo(o2.getSemiPreciousStone().name());
        }
        return o1.getSemiPreciousStone().name().compareTo(o2.getGemStone().name());
    }
}
