package entity;

public class StoneTypes {
    public enum GemStones {
        ALEXANDRITE(8.5, 3.7, 3, "Green", 20000), DIAMOND(10, 3.5, 5, "White", 12000), EMERALD(8, 2.7, 4, "Green", 2000),
        RUBY(9, 4, 4, "Red", 500), SAPPHIRE(9, 3.9, 4, "Blue", 1000);
        private double hardness;
        private double density;
        private int transparency;
        private String color;
        private double pricePerCarat;

        GemStones(double hardness, double density, int transparency, String color, double pricePerCarat) {
            this.hardness = hardness;
            this.density = density;
            this.transparency = transparency;
            this.color = color;
            this.pricePerCarat = pricePerCarat;
        }

        public double getHardness() {
            return hardness;
        }

        public double getDensity() {
            return density;
        }

        public int getTransparency() {
            return transparency;
        }

        public String getColor() {
            return color;
        }

        public double getPricePerCarat() {
            return pricePerCarat;
        }

        @Override
        public String toString() {
            return "Name = " + name() +
                    ", hardness = " + hardness +
                    ", density = " + density +
                    ", transparency = " + transparency +
                    ", color = " + color +
                    ", pricePerCarat = " + pricePerCarat;
        }
    }

    public enum SemiPreciousStones {
        AQUAMARINE(8, 2.6, 3, "Blue", 250), AMETHYST(7, 2.6, 2, "Purple", 100), GARNET(6, 3.8, 2, "Red", 20),
        OPAL(5, 1.5, 1, "Blue", 180), CITRINE(7, 2.2, 2, "Yellow", 200), ZIRCON(6, 4.6, 1, "Pink", 150),
        CHRYSOLITE(6, 3.2, 3, "Green", 300);
        private double hardness;
        private double density;
        private int transparency;
        private String color;
        private double pricePerCarat;

        SemiPreciousStones(double hardness, double density, int transparency, String color, double pricePerCarat) {
            this.hardness = hardness;
            this.density = density;
            this.transparency = transparency;
            this.color = color;
            this.pricePerCarat = pricePerCarat;
        }

        public double getHardness() {
            return hardness;
        }

        public double getDensity() {
            return density;
        }

        public int getTransparency() {
            return transparency;
        }

        public String getColor() {
            return color;
        }

        public double getPricePerCarat() {
            return pricePerCarat;
        }

        @Override
        public String toString() {
            return "Name = " + name() +
                    ", hardness = " + hardness +
                    ", density = " + density +
                    ", transparency = " + transparency +
                    ", color = " + color +
                    ", pricePerCarat = " + pricePerCarat;
        }
    }
}
